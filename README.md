# Project 1: Page Server

Nick Henderson

nhenders@uoregon.edu / nick@nihenderson.com

Simple web server to send contents of .html or .css files with http response, or 
respond with 404 or 403 appropriately. 
